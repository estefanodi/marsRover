# Mars Rover Test

1) the project run in the command line

2) to run the project: cd the main folder and type node mars.js

3) to run the tests: type npm test

## The project is divided in three steps:
 
1) the first shows the empty plateau 

2) the second shows the plateau with the rovers starting position

3) the third step shows the grid with the rover in the final position.
